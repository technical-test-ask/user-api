# User Api

## Description

User-Api is an application written in java / spring boot and used to manage User

# Architecture

The User-Api is composed of :

* a REST API destined to the Front and other microservice
* a data layer to access its own memory database H2

## Requirements

For building and running the application you need:

- [JDK 17 ](https://www.oracle.com/java/technologies/downloads/)
- [Maven 4+](https://maven.apache.org)
- [Spring Tools Suite or Intellij](https://spring.io/tools)
- [git](https://git-scm.com/)

## Dependencies

- [Spring Boot](https://spring.io/projects/spring-boot)
- [MapStruct](https://mapstruct.org/)
- [lombok](https://projectlombok.org/)
- [JUnit](https://junit.org/junit5/docs/current/user-guide/)
- [AssertJ](https://joel-costigliola.github.io/assertj/)

# Code guideline

* we organize java packages like defined
  in [Spring boot Reference Guide](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-structuring-your-code.html)
* we use [lombok](https://projectlombok.org/) plugin to avoid boilerplate code
* we use [MapStruct](http://modelmapper.org/) to simplify and generate the implementation of mappings between java type
  bean types

# Compiling and Testing the java application

You can clone project from repository gitlab

```sh
git clone git@gitlab.com:technical-test-ask/user-api.git
```

**Note** : the maven goal will compile and launch unit/integration tests (avoid skipping them !)

```sh
mvn clean install
```

Compile and launch unit/integration tests

```sh
mvn test
```

**Note** : Each layer (Data,Service,Controller,mapper) has a unit test, all the test Integration use H2 database.

# Application Configuration

A skeleton of the spring configuration file is present in /src/main/resources/application.yml

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in
the fr.astek.user.api.UserApiApplication class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

mvn spring-boot:run

## Persistence model

![img.png](img.png)

## API DOC

you can find user api details in file [api-doc.adoc] . 



