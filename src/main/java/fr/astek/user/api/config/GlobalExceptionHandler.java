package fr.astek.user.api.config;

import fr.astek.user.api.exceptions.UserApiBadRequestException;
import fr.astek.user.api.exceptions.UserApiNotFoundException;
import fr.astek.user.api.exceptions.UserApiServiceException;
import fr.astek.user.api.utils.ApiError;
import fr.astek.user.api.utils.ApiErrorCode;
import fr.astek.user.api.utils.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = UserApiServiceException.class)
    public ResponseEntity<ApiResponse<Object, Object>> serviceException(UserApiServiceException e) {
        log.error(e.getMessage(), e);
        return ApiResponse.error(ApiErrorCode.USER_API_INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UserApiNotFoundException.class)
    public ResponseEntity<ApiResponse<Object, Object>> notFoundException(UserApiNotFoundException e) {
        return ApiResponse.notFound(ApiErrorCode.USER_API_NOTFOUND_ERROR, e.getMessage());
    }

    @ExceptionHandler(value = UserApiBadRequestException.class)
    public ResponseEntity<ApiResponse<Object, Object>> badRequestException(UserApiBadRequestException e) {
        return ApiResponse.badRequest(ApiErrorCode.USER_API_BADREQUEST_ERROR, e.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse<Object, Map<String, String>>> badRequest(MethodArgumentNotValidException e) {
        List<ApiError<String>> badRequestErrors = new LinkedList<>();
        e.getAllErrors().forEach(objectError -> {
            String fieldName = ((FieldError) objectError).getField();
            String errorMessage = objectError.getDefaultMessage();
            badRequestErrors.add(new ApiError<>(fieldName, errorMessage));
        });
        return ApiResponse.badRequest(ApiErrorCode.USER_API_BADREQUEST_ERROR, badRequestErrors);
    }

    @ExceptionHandler(value = BindException.class)
    public ResponseEntity<ApiResponse<Object, Map<String, String>>> badRequest(BindException e) {
        List<ApiError<String>> badRequestErrors = new LinkedList<>();
        e.getAllErrors().forEach(objectError -> {
            String fieldName = ((FieldError) objectError).getField();
            String errorMessage = objectError.getDefaultMessage();
            badRequestErrors.add(new ApiError<>(fieldName, errorMessage));
        });
        return ApiResponse.badRequest(ApiErrorCode.USER_API_BADREQUEST_ERROR, badRequestErrors);
    }

}
