package fr.astek.user.api.config;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "fr.astek.user.api.repositories")
@EntityScan("fr.astek.user.api.entities")
@EnableTransactionManagement
public class JpaConfig {
}
