package fr.astek.user.api.config.aop;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class InfoExecutionMonitoringAspect {

    @Around("@annotation(infoExecutionMonitoring)")
    public Object logInfoExcutionMonitoring(ProceedingJoinPoint joinPoint, InfoExecutionMonitoring infoExecutionMonitoring) throws Throwable {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        log.info("call Method: [ " + signature.getMethod() + " ]");
        StringBuilder infoMethod = new StringBuilder("");
        infoMethod.append("infos Params  method [" + signature.getName() + "] : ");
        Arrays.stream(joinPoint.getArgs()).forEach(o -> infoMethod.append("\n" + "*****" + o.toString()));
        log.info(infoMethod.toString());
        var start = System.currentTimeMillis();
        var processName = infoExecutionMonitoring.processName();

        log.warn("[" + processName + "] START");
        var proceed = joinPoint.proceed();

        var executionTime = System.currentTimeMillis() - start;

        log.warn("[" + processName + "] END EXECUTED IN {} MS", executionTime);
        log.info("Method [" + signature.getName() + "] return : " + proceed.toString());


        return proceed;

    }
}
