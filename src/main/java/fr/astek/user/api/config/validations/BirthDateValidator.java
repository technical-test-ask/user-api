package fr.astek.user.api.config.validations;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

import static java.lang.Math.abs;

public class BirthDateValidator implements ConstraintValidator<BirthDateConstraint, LocalDate> {


    @Override
    public void initialize(BirthDateConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(LocalDate localDate, ConstraintValidatorContext constraintValidatorContext) {
        return 18 < (abs(Period.between(LocalDate.now(), localDate).getYears()));
    }
}
