package fr.astek.user.api.config.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = CodeCountryValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CodeCountryConstraint {
    String message() default "Invalide code country : Only French residents are allowed to create an account";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
