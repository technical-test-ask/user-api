package fr.astek.user.api.config.validations;

import fr.astek.user.api.utils.CountryUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CodeCountryValidator implements ConstraintValidator<CodeCountryConstraint, String> {

    @Override
    public void initialize(CodeCountryConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return CountryUtil.isValidISOCountry(s) && "FR".equals(s);
    }
}
