package fr.astek.user.api.controllers;

import fr.astek.user.api.controllers.requestparams.CreateUserParameter;
import fr.astek.user.api.mapper.UserMapper;
import fr.astek.user.api.models.User;
import fr.astek.user.api.services.UserService;
import fr.astek.user.api.utils.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;


    public UserController(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping
    public ResponseEntity<User> registerUser(@Valid @RequestBody CreateUserParameter createUserParameter) {
        return ApiResponse.created(userService.registerUser(userMapper.toCreateUser(createUserParameter)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Integer id) {
        return ApiResponse.success(userService.getUser(id));
    }


}
