package fr.astek.user.api.controllers.requestparams;

import fr.astek.user.api.config.validations.BirthDateConstraint;
import fr.astek.user.api.config.validations.CodeCountryConstraint;
import fr.astek.user.api.enums.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserParameter implements Serializable {


    @NotBlank(message = "username has to be present and not empty")
    private String username;


    @NotNull(message = "birthdate has to be present")
    @BirthDateConstraint
    private LocalDate birthdate;

    @NotBlank(message = "codeCountry has to be present and not empty")
    @CodeCountryConstraint
    private String codeCountry;

    private String phoneNumber;

    private GenderEnum gender;


}
