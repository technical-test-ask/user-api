package fr.astek.user.api.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity(name = "Country")
@Table(name = "countries")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryEntity implements Serializable {

    @Id
    private String code;

    @Column(name = "name", unique = true)
    private String name;
}
