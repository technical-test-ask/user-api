package fr.astek.user.api.enums;

public enum GenderEnum {
    MALE,
    FEMALE,
    UNKNOWN
}
