package fr.astek.user.api.exceptions;

public class UserApiBadRequestException extends RuntimeException {

    public UserApiBadRequestException(final String message) {
        super(message);
    }

}
