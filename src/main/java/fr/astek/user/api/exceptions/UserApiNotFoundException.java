package fr.astek.user.api.exceptions;

public class UserApiNotFoundException extends RuntimeException {

    public UserApiNotFoundException(final String message) {
        super(message);
    }

}
