package fr.astek.user.api.exceptions;

public class UserApiServiceException extends RuntimeException {

    public UserApiServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
