package fr.astek.user.api.mapper;

import fr.astek.user.api.config.CommonMapperConfig;
import fr.astek.user.api.entities.CountryEntity;
import fr.astek.user.api.models.Country;
import org.mapstruct.Mapper;


@Mapper(config = CommonMapperConfig.class)
public interface CountryMapper {

    CountryEntity toCountryEntity(Country source);
}
