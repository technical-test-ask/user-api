package fr.astek.user.api.mapper;

import fr.astek.user.api.config.CommonMapperConfig;
import fr.astek.user.api.controllers.requestparams.CreateUserParameter;
import fr.astek.user.api.entities.UserEntity;
import fr.astek.user.api.exceptions.UserApiBadRequestException;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.models.CreateUser;
import fr.astek.user.api.models.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Locale;

@Mapper(config = CommonMapperConfig.class)
public interface UserMapper {

    UserEntity toUserEntity(CreateUser source);

    User toUser(UserEntity source);


    @Mapping(target = "country", source = "codeCountry")
    CreateUser toCreateUser(CreateUserParameter source);


    default Country toCountry(String codeCountry) {
        try {
            Locale locale = new Locale("", codeCountry);
            return new Country(locale.getCountry(), locale.getDisplayCountry());
        } catch (Exception e) {
            throw new UserApiBadRequestException("no country matches code [" + codeCountry + "]");
        }

    }
}
