package fr.astek.user.api.models;


import fr.astek.user.api.enums.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreateUser implements Serializable {


    private String username;

    private LocalDate birthdate;

    private Country country;

    private String phoneNumber;

    private GenderEnum gender;


}
