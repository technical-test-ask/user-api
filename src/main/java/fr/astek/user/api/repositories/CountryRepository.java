package fr.astek.user.api.repositories;


import fr.astek.user.api.entities.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<CountryEntity, String> {

    Optional<CountryEntity> getCountryByCodeOrName(String code, String name);

}
