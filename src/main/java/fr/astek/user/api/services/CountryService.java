package fr.astek.user.api.services;

import fr.astek.user.api.mapper.CountryMapper;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.repositories.CountryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CountryService {

    private final CountryRepository countryRepository;

    private final CountryMapper countryMapper;


    public CountryService(final CountryRepository countryRepository, final CountryMapper countryMapper) {
        this.countryRepository = countryRepository;
        this.countryMapper = countryMapper;
    }


    public void createCountry(Country country) {
        var existCountry = countryRepository.getCountryByCodeOrName(country.getCode(), country.getName());
        if (!existCountry.isPresent()) {
            countryRepository.save(countryMapper.toCountryEntity(country));
        }

    }
}
