package fr.astek.user.api.services;


import fr.astek.user.api.config.aop.InfoExecutionMonitoring;
import fr.astek.user.api.exceptions.UserApiNotFoundException;
import fr.astek.user.api.exceptions.UserApiServiceException;
import fr.astek.user.api.mapper.UserMapper;
import fr.astek.user.api.models.CreateUser;
import fr.astek.user.api.models.User;
import fr.astek.user.api.repositories.UserRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final CountryService countryService;

    private final UserMapper userMapper;


    public UserService(final UserRepository userRepository, final CountryService countryService, final UserMapper userMapper) {
        this.userRepository = userRepository;
        this.countryService = countryService;
        this.userMapper = userMapper;
    }

    /**
     * @param createUser
     * @return
     */
    @InfoExecutionMonitoring(processName = "REGISTER_USER")
    public User registerUser(CreateUser createUser) {
        try {
            countryService.createCountry(createUser.getCountry());
            return userMapper.toUser(userRepository.save(userMapper.toUserEntity(createUser)));
        } catch (final DataAccessException e) {
            throw new UserApiServiceException("an error occurred while saving User", e);
        }

    }

    @InfoExecutionMonitoring(processName = "GET_USER")
    public User getUser(Integer id) {
        try {
            return userMapper.toUser(userRepository.findById(id).orElseThrow(() -> new UserApiNotFoundException("Can not find the entity User (id = " + id + " )")));
        } catch (final DataAccessException e) {
            throw new UserApiServiceException("an error occurred while saving User", e);
        }
    }
}
