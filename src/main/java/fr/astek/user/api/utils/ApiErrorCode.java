package fr.astek.user.api.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ApiErrorCode {

    public final String USER_API_INTERNAL_SERVER_ERROR = "USER_API_INTERNAL_SERVER_ERROR";

    public final String USER_API_NOTFOUND_ERROR = "USER_API_NOTFOUND_ERROR";

    public final String USER_API_BADREQUEST_ERROR = "USER_API_BADREQUEST_ERROR";

}
