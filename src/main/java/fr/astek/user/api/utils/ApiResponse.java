package fr.astek.user.api.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<T, E> {

    private T data;

    private Integer status;

    private ApiError<E> error;

    public static <T, E> ResponseEntity success(final T data) {
        final ApiResponse<T, E> apiResponse = new ApiResponse<>(data, HttpStatus.OK.value(), null);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);

    }

    public static <T, E> ResponseEntity created(final T data) {
        final ApiResponse<T, E> apiResponse = new ApiResponse<>(data, HttpStatus.CREATED.value(), null);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);

    }

    public static <T, E> ResponseEntity error(final String errorCode, final E errorDetails) {
        final ApiResponse<T, E> apiResponse = new ApiResponse<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), new ApiError<E>(errorCode, errorDetails));
        return new ResponseEntity<>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    public static ResponseEntity error(final String errorCode) {
        return error(errorCode);
    }

    public static <T, E> ResponseEntity notFound(final String errorCode, final E errorDetails) {
        final ApiResponse<T, E> apiResponse = new ApiResponse<>(null, HttpStatus.NOT_FOUND.value(), new ApiError<E>(errorCode, errorDetails));
        return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);

    }

    public static <T, E> ResponseEntity badRequest(final String errorCode, final E errorDetails) {
        final ApiResponse<T, E> apiResponse = new ApiResponse<>(null, HttpStatus.BAD_REQUEST.value(), new ApiError<E>(errorCode, errorDetails));
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);

    }
}
