package fr.astek.user.api.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Locale;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CountryUtil {

    private static final Set<String> ISO_COUNTRIES = Set.of(Locale.getISOCountries());

    public static boolean isValidISOCountry(String s) {
        return ISO_COUNTRIES.contains(s);
    }
}
