package fr.astek.user.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.astek.user.api.config.GlobalExceptionHandler;
import fr.astek.user.api.controllers.requestparams.CreateUserParameter;
import fr.astek.user.api.enums.GenderEnum;
import fr.astek.user.api.exceptions.UserApiNotFoundException;
import fr.astek.user.api.mapper.UserMapperImpl;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.models.CreateUser;
import fr.astek.user.api.models.User;
import fr.astek.user.api.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Profile("test")
class UserControllerTest {

    public static final String BIRTHDATE_MESSAGE_ERROR = "Invalide birth date : Only adult residents are allowed to create an account";
    public static final String USERNAME_MESSAGE_ERROR = "Invalide code country : Only French residents are allowed to create an account";
    public static final String COUNTRY_MESSAGE_ERROR = "username has to be present and not empty";
    ObjectMapper mapper;
    @Mock
    private UserService userService;
    private UserController userController;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        userController = new UserController(userService, new UserMapperImpl());
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();
    }


    @Test
    void createUserWithStatusCreatedWhenAllParamatersIsValide() throws Exception {
        var createUserParameter = new CreateUserParameter("salim", LocalDate.parse("1991-04-02"), "FR", "+33123456789", GenderEnum.MALE);
        when(userService.registerUser(isA(CreateUser.class))).thenReturn(new User(1, "salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE));

        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(createUserParameter)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.id", equalTo(1)))
                .andExpect(jsonPath("$.data.username", equalTo("salim")))
                .andExpect(jsonPath("$.data.birthdate[0]", equalTo(1991)))
                .andExpect(jsonPath("$.data.birthdate[1]", equalTo(4)))
                .andExpect(jsonPath("$.data.birthdate[2]", equalTo(2)))
                .andExpect(jsonPath("$.data.country.code", equalTo("FR")))
                .andExpect(jsonPath("$.data.country.name", equalTo("France")))
                .andExpect(jsonPath("$.data.phoneNumber", equalTo("+33123456789")))
                .andExpect(jsonPath("$.data.gender", equalTo("MALE")))
                .andExpect(jsonPath("$.status", equalTo(201)))
                .andExpect(jsonPath("$.error", equalTo(null)));


    }

    @Test
    void createUserWithStatusBadRequestdWhenManyParamatersIsNotValide() throws Exception {
        var createUserParameter = new CreateUserParameter(null, LocalDate.parse("2010-04-02"), "EN", "+33123456789", GenderEnum.MALE);
        when(userService.registerUser(isA(CreateUser.class))).thenReturn(new User(1, "salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE));

        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(createUserParameter)))
                .andExpect(status().isBadRequest())
                //.andExpect(jsonPath("$.data", equalTo(null)))
                .andExpect(jsonPath("$.status", equalTo(400)))
                .andExpect(jsonPath("$.error.code", equalTo("USER_API_BADREQUEST_ERROR")))
                .andExpect(jsonPath("$.error.details", hasSize(3)))
                .andExpect(jsonPath("$.error.details[*].code", containsInAnyOrder("codeCountry", "username", "birthdate")))
                .andExpect(jsonPath("$.error.details[*].details", containsInAnyOrder(COUNTRY_MESSAGE_ERROR, USERNAME_MESSAGE_ERROR, BIRTHDATE_MESSAGE_ERROR)));


    }

    @Test
    void getUserWhenIdIsExist() throws Exception {
        when(userService.getUser(1)).thenReturn(new User(1, "salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE));

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id", equalTo(1)))
                .andExpect(jsonPath("$.data.username", equalTo("salim")))
                .andExpect(jsonPath("$.data.birthdate[0]", equalTo(1991)))
                .andExpect(jsonPath("$.data.birthdate[1]", equalTo(4)))
                .andExpect(jsonPath("$.data.birthdate[2]", equalTo(2)))
                .andExpect(jsonPath("$.data.country.code", equalTo("FR")))
                .andExpect(jsonPath("$.data.country.name", equalTo("France")))
                .andExpect(jsonPath("$.data.phoneNumber", equalTo("+33123456789")))
                .andExpect(jsonPath("$.data.gender", equalTo("MALE")))
                .andExpect(jsonPath("$.status", equalTo(200)))
                .andExpect(jsonPath("$.error", equalTo(null)));


    }

    @Test
    void getUserWhenIdIsNotExist() throws Exception {
        when(userService.getUser(1)).thenThrow(new UserApiNotFoundException("Can not find the entity User (id = 1 )") {
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1"))
                .andExpect(status().isNotFound())
                //.andExpect(jsonPath("$.data", equalTo(null)))
                .andExpect(jsonPath("$.status", equalTo(404)))
                .andExpect(jsonPath("$.error.code", equalTo("USER_API_NOTFOUND_ERROR")))
                .andExpect(jsonPath("$.error.details", equalTo("Can not find the entity User (id = 1 )")));


    }


}
