package fr.astek.user.api.it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.astek.user.api.controllers.requestparams.CreateUserParameter;
import fr.astek.user.api.enums.GenderEnum;
import fr.astek.user.api.models.User;
import fr.astek.user.api.utils.ApiError;
import fr.astek.user.api.utils.ApiResponse;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.tuple;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserIntegrationTest {

    private static RestTemplate restTemplate = null;
    @LocalServerPort
    private int port;
    private String baseUrl = "http://localhost";
    private ObjectMapper mapper;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
    }

    @BeforeEach
    public void setUp() {
        this.mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        baseUrl = baseUrl.concat(":").concat(port + "").concat("/users");
    }

    @Test
    @Sql(statements = "DELETE FROM users",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void createUserReturnObjectCreated() throws JsonProcessingException {

        CreateUserParameter createUserParameter = new CreateUserParameter("salim", LocalDate.parse("1991-04-02"), "FR", "+33123456789", GenderEnum.MALE);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CreateUserParameter> request = new HttpEntity<>(createUserParameter, headers);

        ResponseEntity<String> result = restTemplate.postForEntity(baseUrl, request, String.class);

        ApiResponse<User, Object> body = mapper.readValue(result.getBody(), new TypeReference<ApiResponse<User, Object>>() {
        });

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getStatusCodeValue()).isEqualTo(201);
            softly.assertThat(body.getData().getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(body.getData().getId()).isNotNull();
            softly.assertThat(body.getData().getUsername()).isEqualTo("salim");
            softly.assertThat(body.getData().getCountry().getName()).isEqualTo("France");
            softly.assertThat(body.getData().getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(body.getData().getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(body.getData().getGender()).isEqualTo(GenderEnum.MALE);

        });
    }

    @Test
    void createUserReturnBadrequest() throws JsonProcessingException {

        CreateUserParameter createUserParameter = new CreateUserParameter(null, LocalDate.now().minusYears(10), "EN", "+33123456789", GenderEnum.MALE);

        Response response = given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(createUserParameter)
                .expect().statusCode(HttpStatus.BAD_REQUEST.value())
                .when()
                .post(baseUrl).andReturn();

        ApiResponse<Object, List<ApiError<String>>> body = mapper.readValue(response.getBody().prettyPrint(), new TypeReference<ApiResponse<Object, List<ApiError<String>>>>() {
        });

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(body.getData()).isNull();
            softly.assertThat(body.getStatus()).isEqualTo(400);
            softly.assertThat(body.getError().getCode()).isEqualTo("USER_API_BADREQUEST_ERROR");
            softly.assertThat(body.getError().getDetails()).hasSize(3)
                    .extracting(ApiError::getCode, ApiError::getDetails)
                    .containsExactlyInAnyOrder(
                            tuple("birthdate", "Invalide birth date : Only adult residents are allowed to create an account"),
                            tuple("codeCountry", "Invalide code country : Only French residents are allowed to create an account"),
                            tuple("username", "username has to be present and not empty")
                    );

        });
    }


    @Test
    @Sql(statements = "INSERT INTO countries (code,name) VALUES ('FR','France');" +
            "INSERT INTO users (username,birthdate,country_code,phone_number) VALUES ('salim','1991-04-02','FR','+33123456789');",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM users",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void getUserWhenIdExist() throws JsonProcessingException {

        ResponseEntity<String> result = restTemplate.getForEntity(baseUrl.concat("/{id}"), String.class, 1);

        ApiResponse<User, Object> body = mapper.readValue(result.getBody(), new TypeReference<ApiResponse<User, Object>>() {
        });

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getStatusCodeValue()).isEqualTo(200);
            softly.assertThat(body.getData().getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(body.getData().getId()).isEqualTo(1);
            softly.assertThat(body.getData().getUsername()).isEqualTo("salim");
            softly.assertThat(body.getData().getCountry().getName()).isEqualTo("France");
            softly.assertThat(body.getData().getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(body.getData().getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(body.getData().getGender()).isEqualTo(null);

        });
    }

    @Test
    void getUserWhenIdNotExist() throws JsonProcessingException {

        Response response = given()
                .expect().statusCode(HttpStatus.NOT_FOUND.value())
                .when()
                .get(baseUrl + "/1").andReturn();

        ApiResponse<Object, String> body = mapper.readValue(response.getBody().prettyPrint(), new TypeReference<ApiResponse<Object, String>>() {
        });

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(body.getData()).isNull();
            softly.assertThat(body.getStatus()).isEqualTo(404);
            softly.assertThat(body.getError().getCode()).isEqualTo("USER_API_NOTFOUND_ERROR");
            softly.assertThat(body.getError().getDetails()).isEqualTo("Can not find the entity User (id = 1 )");

        });
    }

}
