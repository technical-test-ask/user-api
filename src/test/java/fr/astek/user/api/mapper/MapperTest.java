package fr.astek.user.api.mapper;

import fr.astek.user.api.entities.CountryEntity;
import fr.astek.user.api.entities.UserEntity;
import fr.astek.user.api.enums.GenderEnum;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.models.CreateUser;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class MapperTest {

    private UserMapper userMapper;

    private CountryMapper countryMapper;

    @BeforeEach
    public void setUp() {
        this.userMapper = new UserMapperImpl();
        this.countryMapper = new CountryMapperImpl();
    }

    @Test
    void toCountryEntity_shouldMapCountryToCountryEntity() {
        var country = new Country("FR", "France");
        var target = countryMapper.toCountryEntity(country);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(target.getCode()).isEqualTo("FR");
            softly.assertThat(target.getName()).isEqualTo("France");
        });

    }

    @Test
    void toCountryEntity_shouldReturnNullwhenSourceIsNull() {
        var target = countryMapper.toCountryEntity(null);
        assertThat(target).isNull();

    }

    @Test
    void toUserEntity_shouldMapCreateUserToUserEntity() {
        var userCreate = new CreateUser("salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE);
        var target = userMapper.toUserEntity(userCreate);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(target.getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(target.getId()).isEqualTo(null);
            softly.assertThat(target.getUsername()).isEqualTo("salim");
            softly.assertThat(target.getCountry().getName()).isEqualTo("France");
            softly.assertThat(target.getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(target.getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(target.getGender()).isEqualTo(GenderEnum.MALE);

        });

    }

    @Test
    void toUserEntity_shouldReturnNullwhenSourceIsNull() {
        var target = userMapper.toUserEntity(null);
        assertThat(target).isNull();

    }

    @Test
    void toUser_shouldMapUserEntityToUser() {
        var userEntity = new UserEntity(1, "salim", LocalDate.parse("1991-04-02"), new CountryEntity("FR", "France"), "+33123456789", GenderEnum.MALE);
        var target = userMapper.toUser(userEntity);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(target.getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(target.getId()).isEqualTo(1);
            softly.assertThat(target.getUsername()).isEqualTo("salim");
            softly.assertThat(target.getCountry().getName()).isEqualTo("France");
            softly.assertThat(target.getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(target.getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(target.getGender()).isEqualTo(GenderEnum.MALE);

        });

    }

    @Test
    void toUser_shouldReturnNullwhenSourceIsNull() {
        var target = userMapper.toUser(null);
        assertThat(target).isNull();

    }

}
