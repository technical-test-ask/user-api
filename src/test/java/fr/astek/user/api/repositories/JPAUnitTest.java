package fr.astek.user.api.repositories;


import fr.astek.user.api.entities.CountryEntity;
import fr.astek.user.api.entities.UserEntity;
import fr.astek.user.api.enums.GenderEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class JPAUnitTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryRepository countryRepository;


    @Test
    void should_find_no_users_if_repository_is_empty() {
        Iterable users = userRepository.findAll();
        assertThat(users).isEmpty();
    }

    @Test
    void should_store_a_user_after_store_contry() {
        CountryEntity country = countryRepository.save(CountryEntity.builder().code("fr").name("France").build());
        assertThat(country).hasFieldOrPropertyWithValue("code", "fr")
                .hasFieldOrPropertyWithValue("name", "France");


        UserEntity user = userRepository.save(UserEntity.builder().username("salim").birthdate(LocalDate.parse("1991-06-01")).phoneNumber("+33123456789").gender(GenderEnum.MALE).country(new CountryEntity("fr", "France")).build());

        assertThat(user).hasFieldOrPropertyWithValue("username", "salim")
                .hasFieldOrPropertyWithValue("birthdate", LocalDate.parse("1991-06-01"))
                .hasFieldOrPropertyWithValue("phoneNumber", "+33123456789")
                .hasFieldOrPropertyWithValue("gender", GenderEnum.MALE);
        assertThat(user.getCountry()).hasFieldOrPropertyWithValue("code", "fr")
                .hasFieldOrPropertyWithValue("name", "France");
    }

    @Test
    void should_get_country_by_code_or_name() {
        countryRepository.save(CountryEntity.builder().code("fr").name("France").build());
        Optional<CountryEntity> countryByCode = countryRepository.getCountryByCodeOrName("fr", null);

        assertTrue(countryByCode.isPresent());
        assertThat(countryByCode.get()).hasFieldOrPropertyWithValue("name", "France")
                .hasFieldOrPropertyWithValue("code", "fr");

        Optional<CountryEntity> countryByName = countryRepository.getCountryByCodeOrName(null, "France");
        assertTrue(countryByName.isPresent());
        assertThat(countryByName.get()).hasFieldOrPropertyWithValue("name", "France")
                .hasFieldOrPropertyWithValue("code", "fr");

    }

    @Test
    void should_store_a_user_when_contry_not_exist() {

        assertThatThrownBy(() -> userRepository.save(UserEntity.builder().username("salim").birthdate(LocalDate.parse("1991-06-01")).phoneNumber("+33123456789").gender(GenderEnum.MALE).country(new CountryEntity("fr", "France")).build()))
                .isInstanceOf(InvalidDataAccessApiUsageException.class)
                .hasMessageContaining("transient instance must be saved before current operation");
    }
}
