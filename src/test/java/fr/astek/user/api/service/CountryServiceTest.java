package fr.astek.user.api.service;

import fr.astek.user.api.entities.CountryEntity;
import fr.astek.user.api.mapper.CountryMapperImpl;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.repositories.CountryRepository;
import fr.astek.user.api.services.CountryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CountryServiceTest {

    @Mock
    private CountryRepository countryRepository;


    private CountryService countryService;


    @BeforeEach
    void setUp() {
        countryService = new CountryService(countryRepository, new CountryMapperImpl());
    }

    @Test
    void country_shouldNotCreated_ifexist() {
        var countryEntity = new CountryEntity("FR", "France");
        when(countryRepository.getCountryByCodeOrName("FR", "France")).thenReturn(Optional.of(countryEntity));

        countryService.createCountry(new Country("FR", "France"));

        verify(countryRepository, times(0)).save(any(CountryEntity.class));

    }

    @Test
    void country_shouldCreated_ifNotExist() {
        var countryEntity = new CountryEntity("FR", "France");

        when(countryRepository.getCountryByCodeOrName("FR", "France")).thenReturn(Optional.empty());
        when(countryRepository.save(any(CountryEntity.class))).thenReturn(countryEntity);

        countryService.createCountry(new Country("FR", "France"));

        verify(countryRepository, times(1)).save(any(CountryEntity.class));

    }
}
