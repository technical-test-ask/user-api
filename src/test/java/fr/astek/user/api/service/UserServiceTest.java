package fr.astek.user.api.service;

import fr.astek.user.api.entities.CountryEntity;
import fr.astek.user.api.entities.UserEntity;
import fr.astek.user.api.enums.GenderEnum;
import fr.astek.user.api.exceptions.UserApiNotFoundException;
import fr.astek.user.api.exceptions.UserApiServiceException;
import fr.astek.user.api.mapper.UserMapperImpl;
import fr.astek.user.api.models.Country;
import fr.astek.user.api.models.CreateUser;
import fr.astek.user.api.repositories.UserRepository;
import fr.astek.user.api.services.CountryService;
import fr.astek.user.api.services.UserService;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private CountryService countryService;

    private UserService userService;


    @BeforeEach
    void setUp() {
        userService = new UserService(userRepository, countryService, new UserMapperImpl());
    }

    @Test
    void createUser_shouldReturnUser() {
        var createUser = new CreateUser("salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE);
        var userEntityToSave = new UserEntity(1, "salim", LocalDate.parse("1991-04-02"), new CountryEntity("FR", "France"), "+33123456789", GenderEnum.MALE);
        doNothing().when(countryService).createCountry(isA(Country.class));
        when(userRepository.save(isA(UserEntity.class))).thenReturn(userEntityToSave);

        var target = userService.registerUser(createUser);

        verify(userRepository, times(1)).save(any(UserEntity.class));

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(target.getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(target.getId()).isEqualTo(1);
            softly.assertThat(target.getUsername()).isEqualTo("salim");
            softly.assertThat(target.getCountry().getName()).isEqualTo("France");
            softly.assertThat(target.getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(target.getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(target.getGender()).isEqualTo(GenderEnum.MALE);

        });

    }

    @Test
    void createUser_failureWithDataAccessException() {
        var createUser = new CreateUser("salim", LocalDate.parse("1991-04-02"), new Country("FR", "France"), "+33123456789", GenderEnum.MALE);
        var userEntityToSave = new UserEntity(1, "salim", LocalDate.parse("1991-04-02"), new CountryEntity("FR", "France"), "+33123456789", GenderEnum.MALE);
        doNothing().when(countryService).createCountry(isA(Country.class));
        when(userRepository.save(isA(UserEntity.class))).thenThrow(new DataAccessException("") {
        });

        assertThatThrownBy(() -> userService.registerUser(createUser))
                .isInstanceOf(UserApiServiceException.class)
                .hasMessageContaining("an error occurred while saving User");
    }

    @Test
    void getUser_whenIdexist() {
        var userEntityToSave = new UserEntity(1, "salim", LocalDate.parse("1991-04-02"), new CountryEntity("FR", "France"), "+33123456789", GenderEnum.MALE);
        when(userRepository.findById(1)).thenReturn(Optional.of(userEntityToSave));

        var target = userService.getUser(1);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(target.getBirthdate()).isEqualTo(LocalDate.parse("1991-04-02"));
            softly.assertThat(target.getId()).isEqualTo(1);
            softly.assertThat(target.getUsername()).isEqualTo("salim");
            softly.assertThat(target.getCountry().getName()).isEqualTo("France");
            softly.assertThat(target.getCountry().getCode()).isEqualTo("FR");
            softly.assertThat(target.getPhoneNumber()).isEqualTo("+33123456789");
            softly.assertThat(target.getGender()).isEqualTo(GenderEnum.MALE);

        });


    }

    @Test
    void getUser_failurewhenUserNotExist() {
        when(userRepository.findById(1)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.getUser(1))
                .isInstanceOf(UserApiNotFoundException.class)
                .hasMessageContaining("Can not find the entity User (id = 1 )");
    }

}
